# Matrak Dockerfiles

These files allow you to run the Matrak website with very little configuration.
However, because these files are kept in a separate repository there is some
setup required.

**Clone the repo**

Must be cloned next to the other Matrak repos

```
api/
  - ...
website/
  - ...
automated-tests/
  - ...
docker/
  - docker-compose.yml
  - ...
```

**Copy each of the Dockerfiles into the other repositories.**

```
cp Dockerfile-api ../api/Dockerfile  
cp Dockerfile-tests ../automated-test/Dockerfile
cp Dockerfile-website ../website/Dockerfile
```

You can force git to ignore these files in each of the other repos by adding
it to your local ignore file in each of the repos

```
cd ../website
echo Dockerfile >> .git/info/exclude
# Repeat for automated-tests and api
```

**Update the config of each repo**

*website/public/config.php:* `'url'=>'http://api:8888/'`

*website/public/assets/js/configuration.js:* `var env = 'LOCAL';`

**Run the app**

```
docker-compose up
```
The site should now be available at http://localhost:8000

The first time you run it, the command will take a *long* time because it
needs to download ubuntu, apache, node, php etc. After that, it should start
again in a few seconds.

**Run the tests**

To run the tests, you first need to update your client-side config

*website/public/assets/js/configuration.js:*

```
C_API_URL = 'http://api:8888/';
C_PHOTO_URL = 'http://api:8082/';
```

Then you can run `./test.sh` or `./test.sh <feature>` to run a single suite
